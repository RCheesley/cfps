export function headers(user) {
    return {
        Authorization: `Bearer ${user.token.access_token}`
    }
}

export function sortList(myCFPs, field) {
    return myCFPs.sort((a, b) => (a.data[field] > b.data[field] ? 1 : -1));
}