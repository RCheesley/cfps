import { headers } from "./utils"

const url = "/.netlify/functions/aws"

export async function getSignedURL(user, file) {
    return fetch(`${url}?contentType=${file.type}`, {
        method: 'GET',
        headers: headers(user)
    }).then(response => { return response.json() })
}

export async function uploadFile(signedURL, file) {
    return fetch(signedURL.url, {
        method: "PUT",
        body: file,
    }).then(response => { return response.text() })
}