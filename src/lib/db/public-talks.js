const url = "/.netlify/functions/public-talks"

export async function userTalks(userId) {
    return fetch(`${url}/${userId}`, {
        method: "GET"
    }).then(response => { return response.json() })
}