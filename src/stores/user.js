import { writable } from "svelte/store";
import { navigate } from "svelte-navigator";

export const currentUser = writable({})
export const isLoggedIn = writable(false)

export function login(user, nav = true) {
    netlifyIdentity.refresh() //.then((jwt) => console.log(jwt))
    currentUser.set(user);
    isLoggedIn.set(true);
    netlifyIdentity.close();
    //if (nav) navigate("/home")
}

export function logout(nav = true) {
    console.log('logout store')
    isLoggedIn.set(false);
    currentUser.set({})
    if (nav) navigate("/")
}