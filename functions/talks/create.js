const process = require('process');

const { query, Create, Collection, Paginate, Select, Get, Client } = require('faunadb')

const client = new Client({
    secret: process.env.FAUNADB_SERVER_SECRET,
})

const newTalk = async (event, userId) => {
    const data = JSON.parse(event.body);
    console.log('Function newTalk', data);
    const item = {
        data: { ...data, userId }
    }

    try {
        const response = await client.query(Create(Collection('talks'), item))
        console.log('Success', response);
        return {
            statusCode: 200,
            body: JSON.stringify(response)
        }
    } catch (error) {
        console.error(error);
        return {
            statusCode: 400,
            body: JSON.stringify(error)
        }
    }
}

module.exports = { newTalk }