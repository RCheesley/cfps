const uploadFile = require('./uploadFile')

const handler = async (event, context) => {
    console.log("AWS API");

    try {
        const path = event.path.replace(/\.netlify\/functions\/[^/]+/, '')
        const segments = path.split('/').filter(Boolean);
        console.log(context);
        const { user, identity } = context.clientContext;
        let userId = ''
        if (user) {
            if (user.sub) {
                userId = user.sub
            }
        }

        switch (event.httpMethod) {
            case 'GET':
                return uploadFile.one(userId, event);
            default:
                console.log('Where is my large automobile?');
                break;
        }

    } catch (error) {
        console.log('General error in function');
        console.log('error', error)
        return {
            statusCode: 500,
            body: `There was an error ${error}`,
        }
    }
}

module.exports = { handler }