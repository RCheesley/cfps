const process = require('process');

const { Index, Match, Paginate, Get, Client, Ref, Collection, Call, Function, Select } = require('faunadb')

const client = new Client({
    secret: process.env.FAUNADB_SERVER_SECRET,
})

const all = async (userId) => {
    console.log('Read all CFPs');
    if (userId) {
        try {
            const response = await client.query(Paginate(Match(Index('user_cfps'), userId)))
            const itemRefs = response.data
            const getAllItemsDataQuery = itemRefs.map((ref) => Get(ref))
            const ret = await client.query(getAllItemsDataQuery);
            const body = JSON.stringify(ret)
            return {
                statusCode: 200,
                body
            }
        } catch (error) {
            console.log('error', error)
            return {
                statusCode: 400,
                body: JSON.stringify(error),
            }
        }
    } else {
        return {
            statusCode: 404,
            body: JSON.stringify({ error: 'Not Found'})
        }
    }
}

const one = async (userId, ref) => {
    console.log(`User ${userId} get CFP ${ref}`);

    if (userId) {
        try {
            const response = await client.query(Get(Ref(Collection('cfps'), ref)));
            const body = JSON.stringify(response.data)
            if (response.data.userId === userId) {
                return {
                    statusCode: 200,
                    body
                }
            } else {
                return {
                    statusCode: 401,
                    body: `CFP ${ref} is not associated with user ${userId}`
                }
            }
        } catch (error) {
            console.log('error', error)
            return {
                statusCode: 400,
                body: JSON.stringify(error),
            }
        }
    } else {
        return {
            statusCode: 404,
            body: JSON.stringify({ error: 'Not Found'})
        }
    }
    
}

/**
 * 
 * This function takes a shortCode for a user, looks up their userId using the function userId_from_shortCode
 * With that userId, it get's the user profile.  If that user has enabled their events to be public, we return all events for that user
 * 
 * @param {string} id The shortCode for the user (e.g. from cfps.dev/u/brendan)
 * @returns 
 */
 const public = async (id) => {
    console.log(`Getting public events for ${id}`)
    try {
        const userId = await client.query(Call(Function('userId_from_shortCode'), id));
        console.log(`shortCode ${id} yeilds userId ${userId}, getting profile options`);
        const userProfile = await client.query(Get(Select(["ref"], Get(Match(Index("user_profiles_userid"), userId)))));
        const eventsArePublic = userProfile.data.publishEvents;

        if (eventsArePublic) {
            const response = await client.query(Paginate(Match(Index('user_cfps'), userId)))
            const itemRefs = response.data
            const getAllItemsDataQuery = itemRefs.map((ref) => Get(ref))
            const ret = await client.query(getAllItemsDataQuery);
            const body = JSON.stringify(ret)
            return {
                statusCode: 200,
                body
            }
        } else {
            return {
                statusCode: 418,
                body: JSON.stringify({ error: 'Not public' })
            }
        }
    } catch (error) {
        console.log('There was an error getting the public events')
        console.log('error', error)
        return {
            statusCode: 400,
            body: JSON.stringify({ message: `There was an error getting the public events ${error}` }),
        }
    }
}

module.exports = { all, one, public }