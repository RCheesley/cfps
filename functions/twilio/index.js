const process = require('process');
const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;
const twilioClient = require('twilio')(accountSid, authToken);

const { Index, Match, Paginate, Get, Client, Ref, Collection, Call, Function } = require('faunadb')

const dbClient = new Client({
    secret: process.env.FAUNADB_SERVER_SECRET,
})

const handler = async (event, context) => {
    console.log("Twilio API")

    switch (event.httpMethod) {
        case 'GET':
            try {
                const msg = await twilioClient.messages
                    .create({
                        body: 'Hello trucks from Netlify functions!',
                        messagingServiceSid: 'MG0121d427fc4d07ce0a66033d88377ffb',
                        to: '+14102793957'
                    })
                    .then(message => console.log(message.sid))
                    .done();
                return {
                    statusCode: 200,
                    body: JSON.stringify(msg)
                }
            } catch (error) {
                return {
                    statusCode: 500,
                    body: JSON.stringify(error)
                }
            }
        default:
            return {
                statusCode: '400',
                body: 'Invalid request'
            }
    }
}

module.exports = { handler }