const getTeams = require("./get");
const create = require("./create");
const updateCFPs = require("./update");
const deleteCFPs = require("./delete");

const handler = async (event, context) => {
  const path = event.path.replace(/\.netlify\/functions\/[^/]+/, "");
  const segments = path.split("/").filter(Boolean);

  try {
    const { user, identity } = context.clientContext;
    let userId = "";
    if (user) {
      if (user.sub) {
        userId = user.sub;
      }
    }
    switch (event.httpMethod) {
      case "GET":
        if (segments.length === 0) return getTeams.all(userId);
        if (segments.length === 1) {
          const [id] = segments;
          return getCFPs.one(userId, id);
        }
        if (segments.length === 2) {
          try {
            console.log("Two segments", segments);
            const [id, dir] = segments;
            switch (dir) {
              case "public":
                console.log(`Getting all public events for ${id}`);
                return getCFPs.public(id);
              default:
                return {
                  statusCode: 404,
                  body: "Not found dir",
                };
            }
          } catch (error) {
            console.log(error);
            return {
              statusCode: 400,
              body: JSON.stringify(error),
            };
          }
        }
        return {
          statusCode: 400,
          body: "Too many segments in GET request.  Either / or /:id required",
        };
      case "POST":
        return create.newTeam(event, userId);
      case "PUT":
        //TODO
        if (segments.length === 1) {
          const [id] = segments;
          return updateCFPs.one(userId, id, event);
        }
        return {
          statusCode: 400,
          body: "Invalid segments.  Must be of the form /:id",
        };
      case "DELETE":
        //TODO
        if (segments.length === 1) {
          const [id] = segments;
          return deleteCFPs.one(userId, id);
        }
        return {
          statusCode: 400,
          body: "Invalid segments.  Must be of the form /:id",
        };
      default:
        console.log("Where is my large automobile?");
        break;
    }
  } catch (error) {
    console.log("error", error);
    return {
      statusCode: 500,
      body: JSON.stringify(error),
    };
  }
};

module.exports = { handler };
