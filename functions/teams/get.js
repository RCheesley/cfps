const process = require("process");

const {
  Index,
  Match,
  Paginate,
  Get,
  Client,
  Ref,
  Collection,
  Call,
  Function,
  Select,
} = require("faunadb");

const client = new Client({
  secret: process.env.FAUNADB_SERVER_SECRET,
});

const all = async (userId) => {
  console.log("Read all user Teams");
  if (userId) {
    try {
      console.log(`Get all user teams for ${userId}`);
      const response = await client.query(
        Paginate(Match(Index("teams_members"), userId))
      );
      const itemRefs = response.data;
      const getAllItemsDataQuery = itemRefs.map((ref) => Get(ref));
      const ret = await client.query(getAllItemsDataQuery);
      const body = JSON.stringify(ret);
      return {
        statusCode: 200,
        body,
      };
    } catch (error) {
      console.log("error", error);
      return {
        statusCode: 400,
        body: JSON.stringify(error),
      };
    }
  } else {
    console.error("No userId when getting all user teams");
    return {
      statusCode: 404,
      body: JSON.stringify({ error: "Not Found" }),
    };
  }
};

module.exports = { all };
